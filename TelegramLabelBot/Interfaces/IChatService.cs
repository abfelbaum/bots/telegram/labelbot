using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using TelegramLabelBot.Database.Entities;

namespace TelegramLabelBot.Interfaces;

public interface IChatService
{
    public Task<TelegramChat> GetChat(Chat chat, CancellationToken cancellationToken = new());
}