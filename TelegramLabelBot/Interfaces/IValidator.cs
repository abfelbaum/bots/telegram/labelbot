namespace TelegramLabelBot.Interfaces;

public interface IValidator<in T>
{
    public IValidationResult Validate(T obj);
}