using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using TelegramLabelBot.Database.Entities;

namespace TelegramLabelBot.Interfaces;

public interface ILabelService
{
    public Task SetLabel(string label, User user, Chat chat, long? executingUserId = null, CancellationToken cancellationToken = new());
    public Task<TelegramLabel?> GetLabel(TelegramUser user, CancellationToken cancellationToken = new());

    public Task<bool> RemoveLabel(User user, Chat chat, long? executingUserId = null, CancellationToken cancellationToken = new());

    public Task<bool> RemoveLabel(TelegramUser user, TelegramChat chat, long? executingUserId = null,
        CancellationToken cancellationToken = new());

    public IValidationResult ValidateSet(string label, long? userId = null, long? executingUserId = null);
    public IValidationResult ValidateRemove(long userId, long? executingUserId = null);
}