using System.Collections.Generic;
using Telegram.Bot.Types;
using TelegramLabelBot.Entities;

namespace TelegramLabelBot.Interfaces;

public interface IChatState
{
    public List<ChatMemberAdministrator> Administrators { get; set; }
    public ChatMemberOwner? Owner { get; set; }
    public List<IChatUpdate> Updates { get; set; }

    public void Save(LabelBotContext context);
    public void Apply(LabelBotContext context);
}