using Telegram.Bot.Types;

namespace TelegramLabelBot.Interfaces;

public interface IChatStateProvider
{
    public IChatState GetOrInit(ChatId chatId);
}