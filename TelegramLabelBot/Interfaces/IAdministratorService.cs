using Telegram.Bot.Types;

namespace TelegramLabelBot.Interfaces;

public interface IAdministratorService
{
    public bool IsChatAdmin(User user);
    public bool IsChatAdmin(long userId);
}