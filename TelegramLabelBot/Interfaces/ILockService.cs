namespace TelegramLabelBot.Interfaces;

public interface ILockService
{
    public bool IsLocked(long userId);
    public void Lock(long userId);
}