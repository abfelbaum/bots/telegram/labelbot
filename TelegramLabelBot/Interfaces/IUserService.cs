using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using TelegramLabelBot.Database.Entities;

namespace TelegramLabelBot.Interfaces;

public interface IUserService
{
    public Task<TelegramUser> GetUser(User user, CancellationToken cancellationToken = new());
}