using System.Collections.Generic;
using System.Text;
using TelegramLabelBot.Entities;

namespace TelegramLabelBot.Interfaces;

public interface IValidationResult
{
    public ValidationStatus Status { get; }
    public IEnumerable<string> Reasons { get; }
}