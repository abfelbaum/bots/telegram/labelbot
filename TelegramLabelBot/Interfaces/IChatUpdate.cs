using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using TelegramLabelBot.Entities.LabelUpdates;

namespace TelegramLabelBot.Interfaces;

public interface IChatUpdate
{
    public User User { get; }
    public ChatId ChatId { get; }
    public LabelUpdateType Type { get; }
    public virtual IEnumerable<Type> DependsOn => System.Type.EmptyTypes;
    public Task Apply(ITelegramBotClient client, CancellationToken cancellationToken = new());
    public bool WaitFor(Update update);
    public virtual Task OnFailure(Exception exception, ITelegramBotClient client, CancellationToken cancellationToken = new()) => Task.CompletedTask;
}