using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace TelegramLabelBot.Interfaces;

public interface IAdministratorUpdateTracker
{
    public Task Reset(CancellationToken cancellationToken = new());
    public Task Push(User user, CancellationToken cancellationToken = new());
    public Task ChatMemberUpdated(ChatMemberUpdated chatMember, CancellationToken cancellationToken = new());
}