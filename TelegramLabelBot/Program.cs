﻿using Abfelbaum.Telegram.Bot.Framework.Commands.Extensions;
using Abfelbaum.Telegram.Bot.Framework.Extensions;
using Abfelbaum.Telegram.Bot.Framework.Hosting;
using Abfelbaum.Telegram.Bot.Framework.Polling;
using Abfelbaum.Telegram.Bot.Framework.RateLimiting;
using Abfelbaum.Telegram.Bot.Framework.RequestCulture.Extensions;
using Abfelbaum.Telegram.Bot.Framework.SequentialInputs.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Telegram.Bot.Types.Enums;
using TelegramLabelBot.Database;
using TelegramLabelBot.Entities;
using TelegramLabelBot.Interfaces;
using TelegramLabelBot.Middlewares;
using TelegramLabelBot.Services;
using TelegramLabelBot.Services.Startup;
using TelegramLabelBot.Services.Updates.Options;

TelegramApplicationBuilder<LabelBotContext> builder = TelegramApplication<LabelBotContext>.CreateBuilder(new TelegramApplicationOptions
{
    Args = args,
    ApplicationName = "Telegram Label Bot"
});

builder.Host.UseConsoleLifetime();

// builder.Logging.AddJsonConsole(options =>
// {
//     options.IncludeScopes = true;
//     options.UseUtcTimestamp = true;
//     options.TimestampFormat = "O";
//     options.JsonWriterOptions = new JsonWriterOptions
//     {
//         Indented = true,
//         SkipValidation = false
//     };
// });

builder.Logging.AddFilter("Microsoft.EntityFrameworkCore.Database.Command", LogLevel.Warning);
builder.Logging.AddFilter("Microsoft.EntityFrameworkCore.Infrastructure", LogLevel.Warning);
builder.Logging.AddFilter("Microsoft.EntityFrameworkCore.Database.Connection", LogLevel.Information);
builder.Logging.AddFilter("Microsoft", LogLevel.Information);
builder.Logging.AddFilter("System.Net.Http.HttpClient", LogLevel.Warning);

builder.Configuration.AddCommandLine(args);
builder.Configuration.AddEnvironmentVariables();
builder.Configuration.AddJsonFile("appsettings.json", true);
builder.Configuration.AddJsonFile($"appsettings.{builder.Environment.EnvironmentName}.json", true);

builder.Services.Configure<BotOptions>(builder.Configuration.GetSection("Bot"));

builder.Services.AddBot(builder.Configuration.GetSection("Bot"));
builder.Services.AddDbContext<LabelContext>(contextBuilder =>
{
    contextBuilder.UseNpgsql(builder.Configuration.GetConnectionString("Npgsql"));
});

builder.Services.AddHostedService<DatabaseMigrationService>();
builder.Services.AddRequestScheduler();
builder.Services.AddHostedService<ConsoleTitleService>();
builder.Services.AddHostedService<DatabaseCleanupService>();
builder.Services.AddHostedService<AdministratorUpdateExecutor>();
builder.Services.AddHostedService<AdministratorResetService>();
builder.Services.AddCommandUpdater();
builder.Services.AddPolling();

builder.Services.AddRequestCulture();
builder.Services.AddConcurrentUpdates();
builder.Services.AddSequentialInputs();

builder.Services.AddTransient<IAdministratorUpdateTracker, AdministratorUpdateTracker>();
builder.Services.AddTransient<IAdministratorService, AdministratorService>();
builder.Services.AddTransient<IUserService, UserService>();
builder.Services.AddTransient<IChatService, ChatService>();
builder.Services.AddTransient<ILabelService, LabelService>();
builder.Services.AddSingleton<IChatStateProvider, ChatStateProvider>();
builder.Services.AddSingleton<ILockService, LockService>();
builder.Services.AddSingleton<ChatUpdates>();

builder.AddUpdate<BotMemberUpdateSettings>(UpdateType.MyChatMember);
builder.AddUpdate<InlineQueryUpdateSettings>(UpdateType.InlineQuery);
builder.AddUpdate<ChatMemberLabelUpdateSettings>(UpdateType.ChatMember);
builder.AddUpdate<ChatMemberMessageUpdateSettings>(UpdateType.Message);
builder.AddUpdate<CallbackSettings>(UpdateType.CallbackQuery);
builder.AddUpdate<ChatMemberUpdateSettings>(UpdateType.ChatMember);

builder.AddHelpCommand();
builder.AddCommand<LabelCommandSettings>();
builder.AddCommand<RemoveLabelCommandSettings>();
builder.AddCommand<ResetCommandSettings>();
builder.AddCommand<VerifyCommandSettings>();
builder.AddCommand<VersionCommandSettings>();
builder.AddCommand<LockCommandSettings>();

TelegramApplication<LabelBotContext> app = builder.Build();

app.UseRequestCulture();
app.UseSequentialInputs();
app.UseConcurrentUpdates();
app.Use<ContextStateMiddleware>();
app.UseUpdateHandling();

await app.RunAsync().ConfigureAwait(false);