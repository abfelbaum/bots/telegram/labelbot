using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Telegram.Bot.Types;
using TelegramLabelBot.Database;
using TelegramLabelBot.Database.Entities;
using TelegramLabelBot.Entities;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Services;

public class LabelService : ILabelService
{
    private readonly IChatService _chatService;
    private readonly IUserService _userService;
    private readonly ILockService _lockService;
    private readonly IAdministratorService _administratorService;
    private readonly LabelContext _dbContext;
    private readonly LabelBotContext _botContext;

    public LabelService(LabelContext dbContext, LabelBotContext botContext, IChatService chatService,
        IUserService userService, ILockService lockService, IAdministratorService administratorService)
    {
        _dbContext = dbContext;
        _botContext = botContext;
        _chatService = chatService;
        _userService = userService;
        _lockService = lockService;
        _administratorService = administratorService;
    }

    public async Task<TelegramLabel?> GetLabel(TelegramUser user, CancellationToken cancellationToken = new())
    {
        if (_botContext.Chat is null)
        {
            throw new ArgumentNullException(nameof(_botContext.Chat));
        }

        TelegramChat dbChat = await _chatService.GetChat(_botContext.Chat, cancellationToken).ConfigureAwait(false);

        TelegramLabel? dbLabel =
            await _dbContext.Labels.FirstOrDefaultAsync(x => (x.User == user) && (x.Chat == dbChat), cancellationToken)
                .ConfigureAwait(false);

        return dbLabel;
    }

    public IValidationResult ValidateSet(string label, long? userId = null, long? executingUserId = null)
    {
        List<string> results = new();

        if (string.IsNullOrWhiteSpace(label))
        {
            results.Add("Label cannot be empty");
        }

        label = Trim(label);

        if (string.IsNullOrWhiteSpace(label))
        {
            results.Add("Label cannot be empty");
        }

        if (label.Length > 16)
        {
            results.Add("Label must not be longer than 16 characters");
        }

        if (!label.Any(c => CharRanges.Ranges.Any(r => (r.Min <= c) && (r.Max >= c))))
        {
            results.Add("Label contains invalid symbols");
        }

        if (userId is null)
        {
            return results.Any() ? ValidationResult.Failed(results) : ValidationResult.Success();
        }

        if (executingUserId is null)
        {
            executingUserId = userId;
        }

        if (userId != executingUserId)
        {
            if (!_administratorService.IsChatAdmin(executingUserId.Value))
            {
                results.Add("You are not a chat administrator (if you want to set your own label please do not reply to a message)");
            }
        }

        if (_lockService.IsLocked(userId.Value) && !_administratorService.IsChatAdmin(executingUserId.Value))
        {
            results.Add("Your label is currently locked");
        }

        return results.Any() ? ValidationResult.Failed(results) : ValidationResult.Success();
    }

    public IValidationResult ValidateRemove(long userId, long? executingUserId = null)
    {
        List<string> results = new();

        if (executingUserId is null)
        {
            executingUserId = userId;
        }

        if (userId != executingUserId)
        {
            if (!_administratorService.IsChatAdmin(executingUserId.Value))
            {
                results.Add("You are not a chat administrator (if you want to set your own label please do not reply to a message)");
            }
        }

        if (_lockService.IsLocked(userId) && !_administratorService.IsChatAdmin(executingUserId.Value))
        {
            results.Add("Your label is currently locked");
        }

        return results.Any() ? ValidationResult.Failed(results) : ValidationResult.Success();
    }

    private static string Trim(string label)
    {
        if (label.StartsWith('<') && label.EndsWith('>'))
        {
            return label.Substring(1, label.Length - 2);
        }

        return label;
    }

    public async Task SetLabel(string label, User user, Chat chat, long? executingUserId = null, CancellationToken cancellationToken = new())
    {
        IValidationResult validationResult = ValidateSet(label, user.Id, executingUserId);

        if (validationResult.Status == ValidationStatus.Failed)
        {
            throw new ArgumentException("Invalid label format", nameof(label))
            {
                Data = { { nameof(validationResult), validationResult.Reasons } }
            };
        }

        label = label.Trim();

        TelegramUser dbUser = await _userService.GetUser(user, cancellationToken).ConfigureAwait(false);
        TelegramChat dbChat = await _chatService.GetChat(chat, cancellationToken).ConfigureAwait(false);

        if (!await _dbContext.Entry(dbUser)
                .Collection(x => x.Chats)
                .Query()
                .AnyAsync(x => x == dbChat, cancellationToken)
                .ConfigureAwait(false)
           )
        {
            dbUser.Chats.Add(dbChat);
        }

        TelegramLabel? dbLabel = await GetLabel(dbUser, cancellationToken).ConfigureAwait(false);

        if (dbLabel is null)
        {
            dbLabel = new TelegramLabel
            {
                Chat = dbChat,
                User = dbUser,
            };

            await _dbContext.Labels.AddAsync(dbLabel, cancellationToken).ConfigureAwait(false);
        }

        dbLabel.Label = label;

        await _dbContext.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
    }

    public async Task<bool> RemoveLabel(User user, Chat chat, long? executingUserId = null, CancellationToken cancellationToken = new())
    {
        TelegramUser telegramUser = await _userService.GetUser(user, cancellationToken).ConfigureAwait(false);
        TelegramChat telegramChat = await _chatService.GetChat(chat, cancellationToken).ConfigureAwait(false);

        return await RemoveLabel(telegramUser, telegramChat, executingUserId, cancellationToken).ConfigureAwait(false);
    }

    public async Task<bool> RemoveLabel(TelegramUser user, TelegramChat chat, long? executingUserId = null,
        CancellationToken cancellationToken = new())
    {
        if (executingUserId is null)
        {
            executingUserId = user.Id;
        }

        IValidationResult validationResult = ValidateRemove(user.Id, executingUserId);

        if (validationResult.Status == ValidationStatus.Failed)
        {
            throw new ArgumentException("Invalid label removal")
            {
                Data = { { nameof(validationResult), validationResult.Reasons } }
            };
        }

        TelegramLabel? label = await _dbContext.Labels
            .FirstOrDefaultAsync(l => (l.User == user) && (l.Chat == chat), cancellationToken: cancellationToken)
            .ConfigureAwait(false);

        if (label is null)
        {
            return false;
        }

        _dbContext.Labels.Remove(label);
        await _dbContext.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
        return true;
    }
}