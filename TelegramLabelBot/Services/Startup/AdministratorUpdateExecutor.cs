using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.SequentialInputs.Extensions;
using Abfelbaum.Telegram.Bot.Framework.SequentialInputs.Interfaces;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Telegram.Bot;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Services.Startup;

public class AdministratorUpdateExecutor : BackgroundService
{
    private readonly ITelegramBotClient _client;
    private readonly ILogger<AdministratorUpdateExecutor> _logger;
    private readonly ISequentialInputService _sequentialInput;
    private readonly ChatUpdates _updates;
    private CancellationToken _cancellationToken = CancellationToken.None;
    private bool _isRunning;

    public AdministratorUpdateExecutor(ITelegramBotClient client,
        ILogger<AdministratorUpdateExecutor> logger, ISequentialInputService sequentialInput, ChatUpdates updates)
    {
        _client = client;
        _logger = logger;
        _sequentialInput = sequentialInput;
        _updates = updates;
        _updates.OnEnqueued += OnEnqueued;
    }

    private void OnEnqueued(object? sender, IChatUpdate update)
    {
        if (_isRunning)
        {
            return;
        }

        _ = Update();
    }

    private async Task Update()
    {
        if (_isRunning)
        {
            return;
        }

        _isRunning = true;

        try
        {
            var counter = 0;
            while (!_updates.IsEmpty)
            {
                counter++;

                if (counter == int.MaxValue)
                {
                    _logger.LogError(
                        "Maximum amount of iterations reached ({MaxValue}). This may be because of circular dependencies",
                        int.MaxValue
                    );
                    return;
                }

                if (!_updates.TryDequeue(out IChatUpdate? update))
                {
                    continue;
                }

                if (_updates.Any(u => update.DependsOn.Any(d => d.IsAssignableTo(u.GetType()))))
                {
                    _updates.Enqueue(update);
                    continue;
                }

                try
                {
                    Task waitFor = _sequentialInput.WaitFor(
                        u => update.WaitFor(u),
                        TimeSpan.FromMinutes(1),
                        false,
                        _cancellationToken
                    );

                    await update.Apply(_client, _cancellationToken).ConfigureAwait(false);

                    await waitFor.ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    _logger.LogWarning(e, "Update failed: {Update}", update.GetType().FullName);
                    await update.OnFailure(e, _client, _cancellationToken).ConfigureAwait(false);
                    continue;
                }

                _logger.LogTrace("Update applied: {Update} {Name}", update.GetType().FullName, update.User.FirstName);
            }
        }
        catch (Exception e)
        {
            _logger.LogCritical(e,
                "A fatal error occured while trying to apply updates. Please contact my developer with the stacktrace!");
        }

        _isRunning = false;
    }

    protected override Task ExecuteAsync(CancellationToken stoppingToken)
    {
        _cancellationToken = stoppingToken;

        if (_isRunning)
        {
            return Task.CompletedTask;
        }

        _ = Update();
        return Task.CompletedTask;
    }

    public override void Dispose()
    {
#pragma warning disable CS8601
        _updates.OnEnqueued -= OnEnqueued;
#pragma warning restore CS8601
        base.Dispose();
    }
}