using System;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Microsoft.Extensions.Hosting;
namespace TelegramLabelBot.Services.Startup;

public class ConsoleTitleService : IHostedService
{
    private readonly IBotService _bot;

    public ConsoleTitleService(IBotService bot)
    {
        _bot = bot;
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        Console.Title = (await _bot.Get().ConfigureAwait(false)).Username;
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }
}