using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Types.Enums;
using TelegramLabelBot.Database;
using TelegramLabelBot.Database.Entities;
using TelegramLabelBot.Entities;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Services.Startup;

public class AdministratorResetService : IHostedService
{
    private readonly ILogger<AdministratorResetService> _logger;
    private readonly IServiceScopeFactory _scopeFactory;

    public AdministratorResetService(IServiceScopeFactory scopeFactory, ILogger<AdministratorResetService> logger)
    {
        _scopeFactory = scopeFactory;
        _logger = logger;
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        using IServiceScope outerScope = _scopeFactory.CreateScope();
        LabelContext dbContext = outerScope.ServiceProvider.GetRequiredService<LabelContext>();
        ITelegramBotClient client = outerScope.ServiceProvider.GetRequiredService<ITelegramBotClient>();
        IChatStateProvider stateProvider = outerScope.ServiceProvider.GetRequiredService<IChatStateProvider>();

        IAsyncEnumerable<TelegramChat> chats = dbContext.Chats.AsAsyncEnumerable();

        await foreach (TelegramChat chat in chats.WithCancellation(cancellationToken).ConfigureAwait(false))
        {
            _logger.LogInformation("Resetting chat {Title} ({Id})...", chat.Title, chat.Id.ToString());
            try
            {
                AsyncServiceScope scope = _scopeFactory.CreateAsyncScope();
                await using ConfiguredAsyncDisposable _ = scope.ConfigureAwait(false);
                LabelBotContext botContext = scope.ServiceProvider.GetRequiredService<LabelBotContext>();
                botContext.Chat = await client.GetChatAsync(chat.Id, cancellationToken: cancellationToken).ConfigureAwait(false);
                IChatState state = stateProvider.GetOrInit(chat.Id);

                if (botContext.Chat?.Type is ChatType.Private or ChatType.Sender or null)
                {
                    continue;
                }

                IAdministratorUpdateTracker tracker =
                    scope.ServiceProvider.GetRequiredService<IAdministratorUpdateTracker>();
                await tracker.Reset(cancellationToken).ConfigureAwait(false);

                state.Save(botContext);
            }
            catch (ApiRequestException exception)
            {
                _logger.Log(LogLevel.Error, exception, "Getting admin list of {Chat} failed", chat.Id);

                if (exception.Message is "Bad Request: CHANNEL_PRIVATE" or "Forbidden: bot was kicked from the supergroup chat")
                {
                    dbContext.Chats.Remove(chat);
                }
            }
        }

        await dbContext.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }
}