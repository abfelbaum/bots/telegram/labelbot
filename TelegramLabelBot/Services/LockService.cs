using System;
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using TelegramLabelBot.Entities;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Services;

public class LockService : ILockService
{
    private readonly BotOptions _options;

    public LockService(IOptions<BotOptions> options)
    {
        _options = options.Value;
    }

    private readonly Dictionary<long, DateTime> _userIds = new();

    public void Lock(long userId)
    {
        _userIds[userId] = DateTime.UtcNow + _options.LockTimeout;
    }

    public bool IsLocked(long userId)
    {
        if (!_userIds.ContainsKey(userId))
        {
            return false;
        }

        if (_userIds[userId] < DateTime.UtcNow)
        {
            _userIds.Remove(userId);
            return false;
        }

        return true;
    }
}