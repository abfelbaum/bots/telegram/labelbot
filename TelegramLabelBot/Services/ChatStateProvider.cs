using System.Collections.Generic;

using Telegram.Bot.Types;
using TelegramLabelBot.Entities;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Services;

public class ChatStateProvider : IChatStateProvider
{
    private readonly Dictionary<ChatId, IChatState> _states = new();

    public IChatState GetOrInit(ChatId chatId)
    {
        if (0 == chatId)
        {
            return new ChatState();
        }

        if (!_states.ContainsKey(chatId))
        {
            _states[chatId] = new ChatState();
        }


        return _states[chatId];
    }
}