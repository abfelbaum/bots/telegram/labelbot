using System;
using System.Collections.Concurrent;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Services;

public class ChatUpdates : ConcurrentQueue<IChatUpdate>
{
    public EventHandler<IChatUpdate> OnEnqueued { get; set; }= null!;
    public new void Enqueue(IChatUpdate update)
    {
        base.Enqueue(update);
        OnEnqueued.Invoke(this, update);
    }
}