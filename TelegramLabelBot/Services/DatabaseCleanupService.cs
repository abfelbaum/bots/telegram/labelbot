using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TelegramLabelBot.Database;
using TelegramLabelBot.Database.Entities;

namespace TelegramLabelBot.Services;

[SuppressMessage("ReSharper", "ConditionIsAlwaysTrueOrFalse")]
public class DatabaseCleanupService : BackgroundService
{
    private readonly IServiceScopeFactory _scopeFactory;

    public DatabaseCleanupService(IServiceScopeFactory scopeFactory)
    {
        _scopeFactory = scopeFactory;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            AsyncServiceScope scope = _scopeFactory.CreateAsyncScope();
            LabelContext labelContext = scope.ServiceProvider.GetRequiredService<LabelContext>();

            await CleanupDb(labelContext, stoppingToken).ConfigureAwait(false);
            await labelContext.SaveChangesAsync(stoppingToken).ConfigureAwait(false);

            await Task.Delay(TimeSpan.FromHours(12), stoppingToken).ConfigureAwait(false);
        }
    }

    private async Task CleanupDb(LabelContext context, CancellationToken stoppingToken = new())
    {
        await foreach (TelegramChat chat in context.Chats.Where(x => !x.Users.Any()).AsAsyncEnumerable()
                           .WithCancellation(stoppingToken).ConfigureAwait(false))
        {
            context.Remove(chat);
        }

        await foreach (TelegramUser user in context.Users.Where(x => !x.Chats.Any()).AsAsyncEnumerable()
                           .WithCancellation(stoppingToken).ConfigureAwait(false))
        {
            context.Remove(user);
        }

        await foreach (TelegramLabel label in context.Labels
                           .Where(x => (x.User == null) || (x.Chat == null) || (x.Label == null)).AsAsyncEnumerable()
                           .WithCancellation(stoppingToken).ConfigureAwait(false))
        {
            context.Remove(label);
        }
    }
}