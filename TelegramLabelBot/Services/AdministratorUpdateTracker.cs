#nullable enable
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using TelegramLabelBot.Database;
using TelegramLabelBot.Database.Entities;
using TelegramLabelBot.Entities;
using TelegramLabelBot.Entities.LabelUpdates;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Services;

public class AdministratorUpdateTracker : IAdministratorUpdateTracker
{
    private readonly IAdministratorService _adminService;
    private readonly IChatService _chatService;
    private readonly ITelegramBotClient _client;
    private readonly LabelBotContext _botContext;
    private readonly LabelContext _dbContext;
    private readonly ILabelService _labelService;
    private readonly ILogger<AdministratorUpdateTracker> _logger;
    private readonly ChatUpdates _chatUpdates;
    private readonly BotOptions _options;
    private readonly IUserService _userService;

    public AdministratorUpdateTracker(ITelegramBotClient client, LabelBotContext botContext,
        IOptions<BotOptions> options, LabelContext dbContext, IAdministratorService adminService,
        IUserService userService, IChatService chatService, ILabelService labelService,
        ILogger<AdministratorUpdateTracker> logger, ChatUpdates chatUpdates)
    {
        _client = client;
        _botContext = botContext;
        _dbContext = dbContext;
        _adminService = adminService;
        _userService = userService;
        _chatService = chatService;
        _labelService = labelService;
        _logger = logger;
        _chatUpdates = chatUpdates;
        _options = options.Value;
    }

    private int Capacity
    {
        get
        {
            var capacity = 50;

            if (_botContext.Owner is not null)
            {
                capacity -= 1;
            }

            capacity -= _botContext.Administrators.Count(x => !x.CanBeEdited);

            capacity -= _options.Buffer;

            return capacity;
        }
    }

    private IReadOnlyList<ChatMemberAdministrator> LabelMembers =>
        _botContext.Administrators.Where(x => x.CanBeEdited).ToList().AsReadOnly();


    public async Task Reset(CancellationToken cancellationToken = new())
    {
        if (_botContext.Chat?.Type is ChatType.Private or ChatType.Sender or null)
        {
            return;
        }

        var list = (await _client.GetChatAdministratorsAsync(_botContext.Chat.Id, cancellationToken)
            .ConfigureAwait(false)).ToList();

        _botContext.Owner = list.FirstOrDefault(x => x is ChatMemberOwner) as ChatMemberOwner;

        if (_botContext.Owner is not null)
        {
            list.Remove(_botContext.Owner);
        }

        _botContext.Administrators.Clear();

        foreach (ChatMemberAdministrator administrator in list.Select(a => (ChatMemberAdministrator)a))
        {
            _botContext.Administrators.Add(administrator);
        }

        TelegramChat chat = await _chatService.GetChat(_botContext.Chat, cancellationToken).ConfigureAwait(false);
        foreach (ChatMemberAdministrator administrator in _botContext.Administrators)
        {
            TelegramUser user = await _userService.GetUser(administrator.User, cancellationToken).ConfigureAwait(false);

            await _dbContext.Entry(user).Collection(x => x.Labels).LoadAsync(cancellationToken).ConfigureAwait(false);

            TelegramLabel? label = await _labelService.GetLabel(user, cancellationToken).ConfigureAwait(false);

            if (administrator.CustomTitle is null)
            {
                if (label?.Label is null)
                {
                    if (administrator.CanBeEdited)
                    {
                        _chatUpdates.Enqueue(
                            new RemoveChatAdministratorUpdate(administrator.User, _botContext.Chat.Id));
                    }

                    if (label is not null)
                    {
                        _dbContext.Labels.Remove(label);
                    }
                }
                else
                {
                    if (administrator.CanBeEdited)
                    {
                        _chatUpdates.Enqueue(new RenameAdminTitleUpdate(administrator.User, _botContext.Chat.Id, label.Label));
                    }
                }

                continue;
            }

            if (label is null)
            {
                label = new TelegramLabel
                {
                    Chat = chat,
                    User = user
                };

                await _dbContext.Labels.AddAsync(label, cancellationToken).ConfigureAwait(false);
            }

            label.Label = administrator.CustomTitle;

            if (await _dbContext.Entry(user).Collection(x => x.Chats).Query()
                    .AnyAsync(x => x == chat, cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                continue;
            }

            await _dbContext.Entry(user).Collection(x => x.Chats).LoadAsync(cancellationToken).ConfigureAwait(false);

            user.Chats.Add(chat);
        }

        await _dbContext.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
    }

    private async Task UpdateChatOwner(ChatMemberOwner member, CancellationToken cancellationToken = new())
    {
        ChatMemberAdministrator? user = _botContext.Administrators.FirstOrDefault(a => member.User.Id == a.User.Id);

        if (user is not null)
        {
            _botContext.Administrators.Remove(user);
        }

        TelegramUser dbUser = await _userService.GetUser(member.User, cancellationToken).ConfigureAwait(false);
        TelegramChat dbChat = await _chatService.GetChat(_botContext.Chat!, cancellationToken).ConfigureAwait(false);
        TelegramLabel? label = await _labelService.GetLabel(dbUser, cancellationToken).ConfigureAwait(false);

        if (member.CustomTitle is null && label is not null)
        {
            _dbContext.Labels.Remove(label);

            return;
        }

        if (label is null)
        {
            label = new TelegramLabel
            {
                Chat = dbChat,
                User = dbUser
            };

            await _dbContext.Labels.AddAsync(label, cancellationToken).ConfigureAwait(false);
        }

        label.Label = member.CustomTitle;
    }

    private async Task UpdateChatAdmin(ChatMember member, bool isNew, CancellationToken cancellationToken = new())
    {
        ChatMemberAdministrator? user = _botContext.Administrators.FirstOrDefault(a => member.User.Id == a.User.Id);

        if (user is not null)
        {
            _botContext.Administrators.Remove(user);
        }

        if (member is not ChatMemberAdministrator memberAdministrator)
        {
            return;
        }

        TelegramUser dbUser =
            await _userService.GetUser(memberAdministrator.User, cancellationToken).ConfigureAwait(false);
        TelegramChat dbChat = await _chatService.GetChat(_botContext.Chat!, cancellationToken).ConfigureAwait(false);
        TelegramLabel? label = await _labelService.GetLabel(dbUser, cancellationToken).ConfigureAwait(false);

        _botContext.Administrators.Add(memberAdministrator);

        if (memberAdministrator.CustomTitle is null && label is not null && label.Label is null)
        {
            _dbContext.Labels.Remove(label);
            return;
        }

        if (isNew)
        {
            return;
        }

        if (label is null)
        {
            label = new TelegramLabel
            {
                Chat = dbChat,
                User = dbUser
            };

            await _dbContext.Labels.AddAsync(label, cancellationToken).ConfigureAwait(false);
        }

        label.Label = memberAdministrator.CustomTitle;
    }

    public async Task ChatMemberUpdated(ChatMemberUpdated member, CancellationToken cancellationToken = new())
    {
        if (member.NewChatMember is ChatMemberOwner memberOwner)
        {
            await UpdateChatOwner(memberOwner, cancellationToken).ConfigureAwait(false);
        }
        else
        {
            await UpdateChatAdmin(member.NewChatMember,
                member.OldChatMember is not ChatMemberAdministrator && member.NewChatMember is ChatMemberAdministrator,
                cancellationToken).ConfigureAwait(false);
        }

        await _dbContext.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
    }

    public async Task Push(User user, CancellationToken cancellationToken = new())
    {
        if (_botContext.Chat?.Type is ChatType.Private or ChatType.Sender or null)
        {
            return;
        }

        if (_adminService.IsChatAdmin(user))
        {
            return;
        }

        TelegramUser dbUser = await _userService.GetUser(user, cancellationToken).ConfigureAwait(false);
        TelegramLabel? label = await _labelService.GetLabel(dbUser, cancellationToken).ConfigureAwait(false);

        ChatMemberAdministrator? adminUser = LabelMembers.FirstOrDefault(x => x.User.Id == user.Id);
        if (adminUser is not null)
        {
            _botContext.Administrators.Remove(adminUser);

            if (label?.Label is null)
            {
                _chatUpdates.Enqueue(new RemoveChatAdministratorUpdate(user, _botContext.Chat.Id));
                return;
            }

            _botContext.Administrators.Add(adminUser);

            if (label.Label != adminUser.CustomTitle)
            {
                _chatUpdates.Enqueue(new RenameAdminTitleUpdate(user, _botContext.Chat.Id, label.Label));
            }

            return;
        }

        if (label?.Label is null)
        {
            return;
        }

        _logger.LogInformation("LabelMembers: {MembersCount} Capacity: {Capacity}", LabelMembers.Count, Capacity);
        var counter = 0;
        int labelMembersCount = LabelMembers.Count;
        while (labelMembersCount >= Capacity)
        {
            _logger.LogInformation("Removing label of {User}", LabelMembers[counter].User.FirstName);
            _chatUpdates.Enqueue(new RemoveChatAdministratorUpdate(LabelMembers[counter].User, _botContext.Chat.Id));
            counter++;
            labelMembersCount--;
        }

        _logger.LogInformation("Adding label for {User}", user);
        _chatUpdates.Enqueue(new AddChatAdministratorUpdate(user, _botContext.Chat.Id));
        _chatUpdates.Enqueue(new RenameAdminTitleUpdate(user, _botContext.Chat.Id, label.Label!));
    }
}