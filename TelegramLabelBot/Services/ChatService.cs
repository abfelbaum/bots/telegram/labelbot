using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Telegram.Bot.Types;
using TelegramLabelBot.Database;
using TelegramLabelBot.Database.Entities;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Services;

public class ChatService : IChatService
{
    private readonly LabelContext _context;

    public ChatService(LabelContext context)
    {
        _context = context;
    }

    public async Task<TelegramChat> GetChat(Chat chat, CancellationToken cancellationToken = new())
    {
        TelegramChat? dbChat = await _context.Chats.FirstOrDefaultAsync(x => x.Id == chat.Id, cancellationToken: cancellationToken).ConfigureAwait(false);

        if (dbChat is null)
        {
            dbChat = TelegramChat.FromChat(chat);
            _context.Chats.Add(dbChat);
        }
        else
        {
            dbChat.Update(chat);
        }

        await _context.SaveChangesAsync(cancellationToken).ConfigureAwait(false);

        return dbChat;
    }
}