using System;
using System.Linq;
using Telegram.Bot.Types;
using TelegramLabelBot.Entities;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Services;

public class AdministratorService : IAdministratorService
{
    private readonly LabelBotContext _context;

    public AdministratorService(LabelBotContext context)
    {
        _context = context;
    }

    public bool IsChatAdmin(long userId)
    {
        if (_context.Owner is not null && (_context.Owner.User.Id == userId))
        {
            return true;
        }

        if (_context.Administrators.Any(x => (x.User.Id == userId) && !x.CanBeEdited))
        {
            return true;
        }

        return false;
    }

    public bool IsChatAdmin(User user)
    {
        if (user is null)
        {
            throw new ArgumentNullException(nameof(user));
        }

        return IsChatAdmin(user.Id);
    }
}