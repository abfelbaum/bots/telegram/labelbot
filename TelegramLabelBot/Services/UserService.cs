using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Telegram.Bot.Types;
using TelegramLabelBot.Database;
using TelegramLabelBot.Database.Entities;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Services;

public class UserService : IUserService
{
    private readonly LabelContext _context;

    public UserService(LabelContext context)
    {
        _context = context;
    }

    public async Task<TelegramUser> GetUser(User user, CancellationToken cancellationToken = new())
    {
        TelegramUser? dbUser = await _context.Users.FirstOrDefaultAsync(x => x.Id == user.Id, cancellationToken: cancellationToken).ConfigureAwait(false);

        if (dbUser is null)
        {
            dbUser = TelegramUser.FromUser(user);
            await _context.Users.AddAsync(dbUser, cancellationToken).ConfigureAwait(false);
        }
        else
        {
            dbUser.Update(user);
        }

        await _context.SaveChangesAsync(cancellationToken).ConfigureAwait(false);

        return dbUser;
    }
}