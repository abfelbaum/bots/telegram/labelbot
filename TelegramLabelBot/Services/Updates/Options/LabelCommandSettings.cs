using System.Collections.Generic;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Types;
using Microsoft.Extensions.Options;
using TelegramLabelBot.Entities;
using TelegramLabelBot.Services.Updates.Handlers;

namespace TelegramLabelBot.Services.Updates.Options;

public class LabelCommandSettings : CommandSettings
{
    private readonly BotOptions _options;

    public LabelCommandSettings(IBotService botService, IScopeMatchingService scopeMatchingService, IOptions<BotOptions> options) : base(botService, scopeMatchingService)
    {
        _options = options.Value;
    }

    public override string Name => _options.CommandName;
    public override string ShortDescription => "Updates the label of a user";

    public override string LongDescription => @"Updates the label of a user.

Updates the label of a user.

When the label is surrounded by < and > these will be trimmed. If you want to use them and the start and end of your label, you can double them.

Here are some examples:
<label -> <label
label> -> label>
<label> -> label
<<label>> -> <label>
<<<label>>> -> <<label>>

Administrators can set the label of an user by replying to them.";

    public override IReadOnlyList<string> Usages =>
        new[] { $"{_options.CommandName}", $"{_options.CommandName} <label>" };

    public override IEnumerable<CommandHandler> Handlers { get; } = new List<CommandHandler>
    {
        new CommandHandler<LabelCommandHandler>(CommandScope.AllGroupChats())
    };
}