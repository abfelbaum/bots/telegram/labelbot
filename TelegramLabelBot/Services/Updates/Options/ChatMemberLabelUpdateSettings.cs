using System;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using TelegramLabelBot.Services.Updates.Handlers;

namespace TelegramLabelBot.Services.Updates.Options;

public class ChatMemberLabelUpdateSettings : Abfelbaum.Telegram.Bot.Framework.Settings.ChatMemberUpdateSettings
{
    protected override Task<Type?> GetUpdateHandlerType(Update update)
    {
        if (update.ChatMember is null)
        {
            return Task.FromResult<Type?>(null);
        }

        return Task.FromResult<Type?>(typeof(LabelChatMemberUpdateHandler));
    }
}