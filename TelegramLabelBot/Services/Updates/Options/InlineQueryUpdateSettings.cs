using System;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using TelegramLabelBot.Services.Updates.Handlers;

namespace TelegramLabelBot.Services.Updates.Options;

public class InlineQueryUpdateSettings : Abfelbaum.Telegram.Bot.Framework.Settings.InlineQueryUpdateSettings
{

    protected override Task<Type?> GetUpdateHandlerType(Update update) => Task.FromResult<Type?>(typeof(InlineSearchUpdateHandler));
}