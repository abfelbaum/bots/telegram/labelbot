using System.Collections.Generic;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Types;
using TelegramLabelBot.Services.Updates.Handlers;

namespace TelegramLabelBot.Services.Updates.Options;

public class VersionCommandSettings : CommandSettings
{
    public VersionCommandSettings(IBotService bot, IScopeMatchingService scopeMatchingService) : base(bot,
        scopeMatchingService)
    {
    }

    public override string Name => "version";
    public override string ShortDescription => "Provides information about the bot";
    public override string LongDescription => "Provides version and license information about the bot.";
    public override IReadOnlyList<string> Usages => new[] { "version" };

    public override IEnumerable<CommandHandler> Handlers { get; } = new List<CommandHandler>
    {
        new CommandHandler<VersionCommandHandler>(CommandScope.Default())
    };
}