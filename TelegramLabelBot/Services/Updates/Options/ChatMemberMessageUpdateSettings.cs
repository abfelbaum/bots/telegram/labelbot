using System;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Settings;
using Telegram.Bot.Types;
using TelegramLabelBot.Services.Updates.Handlers;

namespace TelegramLabelBot.Services.Updates.Options;

public class ChatMemberMessageUpdateSettings : Abfelbaum.Telegram.Bot.Framework.Settings.MessageUpdateSettings
{
    protected override Task<Type?> GetUpdateHandlerType(Update update) =>
        Task.FromResult<Type?>(typeof(MessageUpdateHandler));
}