using System;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Settings;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using TelegramLabelBot.Entities;
using TelegramLabelBot.Services.Updates.Handlers;

namespace TelegramLabelBot.Services.Updates.Options;

public class CallbackSettings : CallbackUpdateSettings
{
    protected override Task<Type?> GetUpdateHandlerType(Update update)
    {
        if (update.Type != UpdateType.CallbackQuery)
        {
            return Task.FromResult<Type?>(null);
        }

        if (update.CallbackQuery is not { } query)
        {
            return Task.FromResult<Type?>(null);
        }

        if (query.Data is null)
        {
            return Task.FromResult<Type?>(null);
        }

        if (query.IsId(CallbackId.SetLabel))
        {
            return Task.FromResult<Type?>(typeof(SetLabelCallbackHandler));
        }

        if (query.IsId(CallbackId.SetCustomLabel))
        {

            return Task.FromResult<Type?>(typeof(SetCustomLabelCallbackHandler));
        }

        return Task.FromResult<Type?>(typeof(FallbackCallbackHandler));

    }
}