using System.Collections.Generic;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Types;
using Microsoft.Extensions.Options;
using TelegramLabelBot.Entities;
using TelegramLabelBot.Services.Updates.Handlers;

namespace TelegramLabelBot.Services.Updates.Options;

public class LockCommandSettings : CommandSettings
{
    private readonly BotOptions _options;

    public LockCommandSettings(IBotService botService, IScopeMatchingService scopeMatchingService, IOptions<BotOptions> options) : base(botService, scopeMatchingService)
    {
        _options = options.Value;
    }

    public override string Name => "lock";
    public override string? ShortDescription => "Locks the label of a user";

    public override string? LongDescription =>
        $"Locks the label of a user for {_options.LockTimeout:g}. As long as the user is locked, they can not edit their label.";

    public override IReadOnlyList<string> Usages { get; } = new[] { "lock (as reply)" };

    public override IEnumerable<CommandHandler> Handlers { get; } = new List<CommandHandler>
    {
        new CommandHandler<LockCommandHandler>(CommandScope.AllChatAdministrators())
    };
}