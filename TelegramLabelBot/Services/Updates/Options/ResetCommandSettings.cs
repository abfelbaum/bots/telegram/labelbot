using System.Collections.Generic;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Types;
using TelegramLabelBot.Services.Updates.Handlers;

namespace TelegramLabelBot.Services.Updates.Options;

public class ResetCommandSettings : CommandSettings
{
    public ResetCommandSettings(IBotService bot, IScopeMatchingService scopeMatchingService) : base(bot, scopeMatchingService)
    {
    }

    public override string Name => "reset";
    public override string ShortDescription => "Reset the local admin list (admins only)";
    public override string LongDescription => "Reset the local admin list (admins only).";
    public override IReadOnlyList<string> Usages => new[] { "reset" };

    public override IEnumerable<CommandHandler> Handlers { get; } = new List<CommandHandler>
    {
        new CommandHandler<ResetCommandHandler>(CommandScope.AllChatAdministrators())
    };
}