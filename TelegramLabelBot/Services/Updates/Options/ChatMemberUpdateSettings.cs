using System;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using TelegramLabelBot.Services.Updates.Handlers;

namespace TelegramLabelBot.Services.Updates.Options;

public class ChatMemberUpdateSettings : Abfelbaum.Telegram.Bot.Framework.Settings.ChatMemberUpdateSettings
{
    protected override Task<Type?> GetUpdateHandlerType(Update update)
    {
        if (update.Type != UpdateType.ChatMember)
        {
            return Task.FromResult<Type?>(null);
        }

        if (update.ChatMember is null)
        {
            return Task.FromResult<Type?>(null);
        }

        if (update.ChatMember.NewChatMember is ChatMemberMember && update.ChatMember.OldChatMember is ChatMemberLeft)
        {
            return Task.FromResult<Type?>(typeof(UserJoinChatMemberUpdateHandler));
        }

        if (update.ChatMember.NewChatMember is ChatMemberBanned or ChatMemberLeft)
        {
            return Task.FromResult<Type?>(typeof(UserLeftChatMemberUpdateHandler));
        }

        return Task.FromResult<Type?>(null);
    }
}