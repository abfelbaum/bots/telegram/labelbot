using System.Collections.Generic;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Types;
using Microsoft.Extensions.Options;
using TelegramLabelBot.Entities;
using TelegramLabelBot.Services.Updates.Handlers;

namespace TelegramLabelBot.Services.Updates.Options;

public class RemoveLabelCommandSettings : CommandSettings
{
    private readonly BotOptions _options;

    public RemoveLabelCommandSettings(IBotService botService, IScopeMatchingService scopeMatchingService,
        IOptions<BotOptions> options) : base(botService, scopeMatchingService)
    {
        _options = options.Value;
    }

    public override string Name => $"remove{_options.CommandName}";
    public override string ShortDescription => "Removes the label of a user";

    public override string LongDescription => @"Removes the label of a user

Invoking this command will delete your current label.

Administrators can remove the label of an user by replying to them.";

    public override IReadOnlyList<string> Usages => new[] { $"{_options.CommandName}" };

    public override IEnumerable<CommandHandler> Handlers { get; } = new List<CommandHandler>
    {
        new CommandHandler<RemoveLabelCommandHandler>(CommandScope.AllGroupChats())
    };
}