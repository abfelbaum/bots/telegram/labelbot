using System.Collections.Generic;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Types;
using TelegramLabelBot.Services.Updates.Handlers;

namespace TelegramLabelBot.Services.Updates.Options;

public class VerifyCommandSettings : CommandSettings
{
    public VerifyCommandSettings(IBotService bot, IScopeMatchingService scopeMatchingService) : base(bot, scopeMatchingService)
    {
    }
    public override string Name => "verify";
    public override string ShortDescription => "Verifies if an user is a real admin";
    public override string LongDescription => $"{ShortDescription}.\n\nPlease use as reply to another user!";
    public override IReadOnlyList<string> Usages { get; } = new[] { "verify (as reply)" };

    public override IEnumerable<CommandHandler> Handlers { get; } = new List<CommandHandler>
    {
        new CommandHandler<VerifyCommandHandler>(CommandScope.AllGroupChats())
    };
}