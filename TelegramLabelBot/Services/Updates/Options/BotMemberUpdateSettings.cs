using System;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Settings;
using Telegram.Bot.Types;
using TelegramLabelBot.Services.Updates.Handlers;

namespace TelegramLabelBot.Services.Updates.Options;

public class BotMemberUpdateSettings : MyChatMemberUpdateSettings
{
    protected override Task<Type?> GetUpdateHandlerType(Update update)
    {
        if (update.MyChatMember is not { } myChatMember)
        {
            return Task.FromResult<Type?>(null);
        }

        if (myChatMember.OldChatMember is ChatMemberLeft && myChatMember.NewChatMember is ChatMemberMember)
        {
            return Task.FromResult<Type?>(typeof(BotJoinUpdateHandler));
        }

        if (myChatMember.NewChatMember is ChatMemberLeft or ChatMemberBanned)
        {
            return Task.FromResult<Type?>(typeof(BotLeftUpdateHandler));
        }

        return Task.FromResult<Type?>(null);
    }
}