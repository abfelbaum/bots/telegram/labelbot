using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Telegram.Bot;
using Telegram.Bot.Types;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Services.Updates.Handlers;

public class ResetCommandHandler : ITelegramUpdate<Message>
{
    private readonly ITelegramBotClient _client;
    private readonly IAdministratorService _adminService;
    private readonly IAdministratorUpdateTracker _updateTracker;

    public ResetCommandHandler(IAdministratorUpdateTracker updateTracker, ITelegramBotClient client,
        IAdministratorService adminService)
    {
        _updateTracker = updateTracker;
        _client = client;
        _adminService = adminService;
    }

    public async Task InvokeAsync(Message message, CancellationToken cancellationToken = new())
    {
        if (message.From is null)
        {
            return;
        }

        long chatId = message.Chat.Id;
        if (!_adminService.IsChatAdmin(message.From))
        {
            await _client.SendTextMessageAsync(chatId, "You do not have permissions to reset the admin list!", replyToMessageId: message.MessageId, cancellationToken: cancellationToken).ConfigureAwait(false);
            return;
        }


        await _updateTracker.Reset(cancellationToken).ConfigureAwait(false);
        await _client.SendTextMessageAsync(chatId, "Admin list reset!", replyToMessageId: message.MessageId, cancellationToken: cancellationToken).ConfigureAwait(false);
    }
}