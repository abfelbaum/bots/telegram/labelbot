using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramLabelBot.Database;
using TelegramLabelBot.Database.Entities;
using TelegramLabelBot.Entities;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Services.Updates.Handlers;

public class UserJoinChatMemberUpdateHandler : ITelegramUpdate<ChatMemberUpdated>
{
    private readonly IChatService _chatService;
    private readonly IUserService _userService;
    private readonly LabelContext _context;
    private readonly ITelegramBotClient _client;
    private readonly BotOptions _options;

    public UserJoinChatMemberUpdateHandler(IChatService chatService, IUserService userService, LabelContext context,
        IOptions<BotOptions> options, ITelegramBotClient client)
    {
        _chatService = chatService;
        _userService = userService;
        _context = context;
        _client = client;
        _options = options.Value;
    }

    public async Task InvokeAsync(ChatMemberUpdated chatMember, CancellationToken cancellationToken = new())
    {
        TelegramChat chat = await _chatService.GetChat(chatMember.Chat, cancellationToken).ConfigureAwait(false);
        TelegramUser user = await _userService.GetUser(chatMember.NewChatMember.User, cancellationToken).ConfigureAwait(false);

        if (!await _context.Entry(user).Collection(x => x.Chats).Query()
                .AnyAsync(x => x == chat, cancellationToken: cancellationToken).ConfigureAwait(false))
        {
            await _context.Entry(user).Collection(x => x.Chats).LoadAsync(cancellationToken).ConfigureAwait(false);

            user.Chats.Add(chat);
        }

        await _context.SaveChangesAsync(cancellationToken).ConfigureAwait(false);

        StringBuilder builder = new();

        builder.Append($"Hey, {chatMember.NewChatMember.User.FirstName}!");
        builder.AppendLine();

        builder.Append(
            $"If you'd like to share you pronouns with us, you can either choose one from the list below or set custom ones!"
        );

        var buttons = _options.DefaultLabels
            .Chunk(2)
            .Select(chunks =>
                chunks.Select(chunk =>
                    InlineKeyboardButton.WithCallbackData(chunk, CallbackUtils.EncodeData(CallbackId.SetLabel, chatMember.NewChatMember.User.Id, chunk))
                ).ToList()
            ).ToList();

        buttons.Add(new List<InlineKeyboardButton>
        {
            InlineKeyboardButton.WithCallbackData("Custom", CallbackId.SetCustomLabel)
        });

        var keyboard = new InlineKeyboardMarkup(buttons);

        await _client.SendTextMessageAsync(chatMember.Chat.Id, builder.ToString(), ParseMode.Html,
                replyMarkup: keyboard, cancellationToken: cancellationToken)
            .ConfigureAwait(false);
    }
}