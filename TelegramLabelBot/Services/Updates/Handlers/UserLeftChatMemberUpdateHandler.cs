using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Microsoft.EntityFrameworkCore;
using Telegram.Bot.Types;
using TelegramLabelBot.Database;
using TelegramLabelBot.Database.Entities;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Services.Updates.Handlers;

public class UserLeftChatMemberUpdateHandler : ITelegramUpdate<ChatMemberUpdated>
{
    private readonly IUserService _userService;
    private readonly IChatService _chatService;
    private readonly LabelContext _context;

    public UserLeftChatMemberUpdateHandler(IUserService userService, IChatService chatService, LabelContext context)
    {
        _userService = userService;
        _chatService = chatService;
        _context = context;
    }

    public async Task InvokeAsync(ChatMemberUpdated chatMember, CancellationToken cancellationToken = new())
    {
        TelegramUser user = await _userService.GetUser(chatMember.OldChatMember.User, cancellationToken).ConfigureAwait(false);
        TelegramChat chat = await _chatService.GetChat(chatMember.Chat, cancellationToken).ConfigureAwait(false);


        TelegramLabel? label = await _context.Entry(user).Collection(x => x.Labels).Query()
            .FirstOrDefaultAsync(x => x.Chat == chat, cancellationToken: cancellationToken).ConfigureAwait(false);

        if (label is not null)
        {
            _context.Labels.Remove(label);
        }

        await _context.Entry(user).Collection(x => x.Chats).LoadAsync(cancellationToken).ConfigureAwait(false);

        user.Chats.Remove(chat);

        if (!user.Chats.Any())
        {
            _context.Users.Remove(user);
        }

        await _context.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
    }
}