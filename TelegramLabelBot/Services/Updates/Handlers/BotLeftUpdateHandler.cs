using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Telegram.Bot.Types;
using TelegramLabelBot.Database;
using TelegramLabelBot.Database.Entities;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Services.Updates.Handlers;

public class BotLeftUpdateHandler : ITelegramUpdate<ChatMemberUpdated>
{
    private readonly IChatService _chatService;
    private readonly LabelContext _context;

    public BotLeftUpdateHandler(LabelContext context, IChatService chatService)
    {
        _context = context;
        _chatService = chatService;
    }

    public async Task InvokeAsync(ChatMemberUpdated chatMember, CancellationToken cancellationToken = new())
    {
        TelegramChat chat = await _chatService.GetChat(chatMember.Chat, cancellationToken).ConfigureAwait(false);

        _context.Chats.Remove(chat);
        await _context.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
    }
}