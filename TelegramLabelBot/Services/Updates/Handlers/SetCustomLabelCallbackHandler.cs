using System;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.SequentialInputs.Extensions;
using Abfelbaum.Telegram.Bot.Framework.SequentialInputs.Interfaces;
using Microsoft.Extensions.Options;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramLabelBot.Entities;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Services.Updates.Handlers;

public class SetCustomLabelCallbackHandler : ITelegramUpdate<CallbackQuery>
{
    private readonly ITelegramBotClient _client;
    private readonly ISequentialInputService _input;
    private readonly BotOptions _options;
    private readonly ILabelService _labelService;
    private readonly IAdministratorUpdateTracker _updateTracker;

    public SetCustomLabelCallbackHandler(ITelegramBotClient client, ISequentialInputService input,
        IOptions<BotOptions> options, ILabelService labelService, IAdministratorUpdateTracker updateTracker)
    {
        _client = client;
        _input = input;
        _options = options.Value;
        _labelService = labelService;
        _updateTracker = updateTracker;
    }

    public async Task InvokeAsync(CallbackQuery query, CancellationToken cancellationToken = new())
    {
        if (query.Data is null || query.Message is null)
        {
            await Error(query, cancellationToken: cancellationToken).ConfigureAwait(false);
            return;
        }

        (long userId, string? _) = query.DecodeData(CallbackId.SetCustomLabel);

        if (userId != query.From.Id)
        {
            await Error(query, "nah", cancellationToken).ConfigureAwait(false);
            return;
        }

        Message message = query.Message;

        try
        {
            await _client.DeleteMessageAsync(message.Chat.Id, message.MessageId, cancellationToken)
                .ConfigureAwait(false);
        }
        catch (Exception)
        {
            // ignore
        }

        Message labelRequestMessage = await _client.SendTextMessageAsync(message.Chat.Id,
                "Please send your label as a text message (timeout 5 minutes)",
                allowSendingWithoutReply: true,
                replyMarkup: new ForceReplyMarkup
                {
                    Selective = true,
                    InputFieldPlaceholder =
                        $"{new string(string.Join(", ", _options.DefaultLabels).Take(61).ToArray())}..."
                }, cancellationToken: cancellationToken)
            .ConfigureAwait(false);

        Message labelMessage = await _input.WaitForMessage(message.Chat.Id, MessageType.Text,
            m => m, TimeSpan.FromMinutes(5), query.From.Id,
            cancellationToken).ConfigureAwait(false);

        StringBuilder builder = await SetLabel(labelMessage.Text!, query.From, message.Chat, cancellationToken)
            .ConfigureAwait(false);

        await _client.SendTextMessageAsync(message.Chat.Id, builder.ToString(), ParseMode.Html,
            replyToMessageId: labelMessage.MessageId, allowSendingWithoutReply: true,
            cancellationToken: cancellationToken).ConfigureAwait(false);

        try
        {
            await _client.DeleteMessageAsync(labelRequestMessage.Chat.Id, labelRequestMessage.MessageId,
                cancellationToken).ConfigureAwait(false);
        }
        catch (Exception)
        {
            // ignore
        }

        await _updateTracker.Push(query.From, cancellationToken).ConfigureAwait(false);
    }

    private async Task<StringBuilder> SetLabel(string label, User user, Chat chat,
        CancellationToken cancellationToken = new())
    {
        var builder = new StringBuilder();
        IValidationResult validationResult = _labelService.ValidateSet(label);

        if (validationResult.Status == ValidationStatus.Failed)
        {
            builder.Append("The label is not valid because of the following reasons:");

            foreach (string reason in validationResult.Reasons)
            {
                builder.AppendLine();
                builder.Append($"- {WebUtility.HtmlEncode(reason)}");
            }

            return builder;
        }

        await _labelService.SetLabel(label, user, chat, cancellationToken: cancellationToken).ConfigureAwait(false);

        builder.Append($"Label <code>{WebUtility.HtmlEncode(label)}</code> for <code>");

        if (user.Username is null)
        {
            builder.Append(WebUtility.HtmlEncode(user.FirstName));
        }
        else
        {
            builder.Append($"@{WebUtility.HtmlEncode(user.Username)}");
        }

        builder.Append("</code> saved!");

        return builder;
    }

    private Task Error(CallbackQuery query, string? message = null, CancellationToken cancellationToken = new())
    {
        return _client.AnswerCallbackQueryAsync(query.Id,
            message ?? "An error occured. Please do not press this button again, the result won't change!",
            true,
            cancellationToken: cancellationToken);
    }
}