using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace TelegramLabelBot.Services.Updates.Handlers;

public class FallbackCallbackHandler : ITelegramUpdate<CallbackQuery>
{
    private readonly ITelegramBotClient _client;

    public FallbackCallbackHandler(ITelegramBotClient client)
    {
        _client = client;
    }

    public Task InvokeAsync(CallbackQuery query, CancellationToken cancellationToken = new()) =>
        _client.AnswerCallbackQueryAsync(query.Id, "Invalid callback", true, cancellationToken: cancellationToken);
}