using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Telegram.Bot.Types;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Services.Updates.Handlers;

public class BotJoinUpdateHandler : ITelegramUpdate<ChatMemberUpdated>
{
    private readonly IAdministratorUpdateTracker _updateTracker;

    public BotJoinUpdateHandler(IAdministratorUpdateTracker updateTracker)
    {
        _updateTracker = updateTracker;
    }

    public async Task InvokeAsync(ChatMemberUpdated args, CancellationToken cancellationToken = new())
    {
        await _updateTracker.Reset(cancellationToken).ConfigureAwait(false);
    }
}