using System;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Extensions;
using Abfelbaum.Telegram.Bot.Framework.SequentialInputs.Extensions;
using Abfelbaum.Telegram.Bot.Framework.SequentialInputs.Interfaces;
using Microsoft.Extensions.Options;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramLabelBot.Entities;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Services.Updates.Handlers;

public class LabelCommandHandler : ITelegramUpdate<Message>
{
    private readonly ITelegramBotClient _client;
    private readonly ILabelService _labelService;
    private readonly ISequentialInputService _sequentialInputService;
    private readonly BotOptions _options;
    private readonly IAdministratorUpdateTracker _updateTracker;

    public LabelCommandHandler(ITelegramBotClient client, IAdministratorUpdateTracker updateTracker,
        ILabelService labelService, ISequentialInputService sequentialInputService, IOptions<BotOptions> options)
    {
        _client = client;
        _updateTracker = updateTracker;
        _labelService = labelService;
        _sequentialInputService = sequentialInputService;
        _options = options.Value;
    }

    public async Task InvokeAsync(Message message, CancellationToken cancellationToken = new())
    {
        StringBuilder builder = new();
        string label = message.GetCommandArguments().Trim();
        User? executingUser = message.From;

        if (executingUser is null)
        {
            return;
        }

        User affectedUser = message.ReplyToMessage?.From ?? executingUser;

        if (!label.Any())
        {
            label = await AskForLabel(message, cancellationToken).ConfigureAwait(false);
        }

        builder.Append(await SetLabel(label, affectedUser, executingUser, message.Chat, cancellationToken).ConfigureAwait(false));
        await _updateTracker.Push(affectedUser, cancellationToken).ConfigureAwait(false);

        await _client.SendTextMessageAsync(message.Chat.Id, builder.ToString(), ParseMode.Html,
            replyToMessageId: message.MessageId, cancellationToken: cancellationToken).ConfigureAwait(false);
    }

    private async Task<StringBuilder> SetLabel(string label, User affectedUser, User executingUser, Chat chat,
        CancellationToken cancellationToken = new())
    {
        var builder = new StringBuilder();
        IValidationResult validationResult = _labelService.ValidateSet(label, affectedUser.Id, executingUser.Id);

        if (validationResult.Status == ValidationStatus.Failed)
        {
            builder.Append("The label is not valid because of the following reasons:");

            foreach (string reason in validationResult.Reasons)
            {
                builder.AppendLine();
                builder.Append($"- {WebUtility.HtmlEncode(reason)}");
            }

            return builder;
        }

        await _labelService.SetLabel(label, affectedUser, chat, executingUser.Id, cancellationToken).ConfigureAwait(false);

        builder.Append($"Label <code>{WebUtility.HtmlEncode(label)}</code> for <code>");

        if (affectedUser.Username is null)
        {
            builder.Append(WebUtility.HtmlEncode(affectedUser.FirstName));
        }
        else
        {
            builder.Append($"@{WebUtility.HtmlEncode(affectedUser.Username)}");
        }

        builder.Append("</code> saved!");

        return builder;
    }

    private async Task<string> AskForLabel(Message message, CancellationToken cancellationToken = new())
    {
        await _client.SendTextMessageAsync(message.Chat.Id,
                "Please send your label as a text message (timeout 5 minutes)", replyToMessageId: message.MessageId, replyMarkup: new ForceReplyMarkup()
                {
                    Selective = true,
                    InputFieldPlaceholder = $"{new string(string.Join(", ", _options.DefaultLabels).Take(61).ToArray())}..."
                }, cancellationToken: cancellationToken)
            .ConfigureAwait(false);

        return await _sequentialInputService.WaitForMessage(message.Chat.Id, MessageType.Text,
            message1 => message1.Text!.Trim(), TimeSpan.FromMinutes(5), message.From?.Id,
            cancellationToken).ConfigureAwait(false);
    }
}