using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Telegram.Bot;
using Telegram.Bot.Types;
using TelegramLabelBot.Entities;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Services.Updates.Handlers;

public class SetLabelCallbackHandler : ITelegramUpdate<CallbackQuery>
{
    private readonly ITelegramBotClient _client;
    private readonly ILabelService _labelService;
    private readonly IAdministratorUpdateTracker _updateTracker;

    public SetLabelCallbackHandler(ITelegramBotClient client, ILabelService labelService,
        IAdministratorUpdateTracker updateTracker)
    {
        _client = client;
        _labelService = labelService;
        _updateTracker = updateTracker;
    }

    public async Task InvokeAsync(CallbackQuery query, CancellationToken cancellationToken = new())
    {
        if (query.Data is null || query.Message is null)
        {
            await Error(query, cancellationToken: cancellationToken).ConfigureAwait(false);
            return;
        }

        (long userId, string? label) = query.DecodeData(CallbackId.SetLabel);

        if (userId != query.From.Id)
        {
            await Error(query, "nah", cancellationToken).ConfigureAwait(false);
            return;
        }

        if (string.IsNullOrWhiteSpace(label))
        {
            await Error(query, cancellationToken: cancellationToken).ConfigureAwait(false);
            return;
        }

        IValidationResult validationResult = _labelService.ValidateSet(label);

        if (validationResult.Status == ValidationStatus.Failed)
        {
            await Error(query, cancellationToken: cancellationToken).ConfigureAwait(false);
            return;
        }

        await _labelService.SetLabel(label, query.From, query.Message.Chat, cancellationToken: cancellationToken)
            .ConfigureAwait(false);

        await _client.AnswerCallbackQueryAsync(query.Id, $"Label {label} saved!", cancellationToken: cancellationToken)
            .ConfigureAwait(false);

        await _client.EditMessageTextAsync(query.Message.Chat.Id, query.Message.MessageId,
                $"{query.From.FirstName} chose the label {label}!", replyMarkup: null,
                cancellationToken: cancellationToken)
            .ConfigureAwait(false);

        await _updateTracker.Push(query.From, cancellationToken).ConfigureAwait(false);
    }

    private Task Error(CallbackQuery query, string? message = null, CancellationToken cancellationToken = new())
    {
        return _client.AnswerCallbackQueryAsync(query.Id,
            message ?? "An error occured. Please do not press this button again, the result won't change!", true,
            cancellationToken: cancellationToken);
    }
}