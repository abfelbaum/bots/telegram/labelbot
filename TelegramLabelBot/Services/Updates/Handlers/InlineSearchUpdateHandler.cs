using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Microsoft.EntityFrameworkCore;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InlineQueryResults;
using TelegramLabelBot.Database;
using TelegramLabelBot.Database.Entities;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Services.Updates.Handlers;

public class InlineSearchUpdateHandler : ITelegramUpdate<InlineQuery>
{
    private readonly ITelegramBotClient _client;
    private readonly LabelContext _context;
    private readonly IUserService _userService;

    public InlineSearchUpdateHandler(LabelContext context, ITelegramBotClient client, IUserService userService)
    {
        _context = context;
        _client = client;
        _userService = userService;
    }

    public async Task InvokeAsync(InlineQuery inlineQuery, CancellationToken cancellationToken = new())
    {

        TelegramUser user = await _userService.GetUser(inlineQuery.From, cancellationToken).ConfigureAwait(false);

        List<InlineQueryResult> results = new();

        results.AddRange(await GetLabels(user, inlineQuery).ToListAsync(cancellationToken).ConfigureAwait(false));

        if (!results.Any())
        {
            string errorTitle;
            string errorDetail;
            if (string.IsNullOrWhiteSpace(inlineQuery.Query))
            {
                errorTitle = "Who are you looking for?";
                errorDetail = "Please type a search query.";
            }
            else
            {
                errorTitle = "Not found";
                errorDetail = "No labels found.";
            }

            results.Add(new InlineQueryResultArticle(Guid.NewGuid().ToString(), errorTitle,
                new InputTextMessageContent(errorDetail)));
        }

        await _client.AnswerInlineQueryAsync(inlineQuery.Id, results,
            isPersonal: true, cacheTime: 0, cancellationToken: cancellationToken).ConfigureAwait(false);
    }

    private IAsyncEnumerable<InlineQueryResult> GetLabels(TelegramUser telegramUser, InlineQuery inlineQuery)
    {
        string query = inlineQuery.Query;

        var labels =
        (
            from chat in _context.Entry(telegramUser).Collection(x => x.Chats).Query()
            from user in chat.Users
            where Regex.IsMatch(user.Firstname, query, RegexOptions.IgnoreCase) ||
                  (user.Lastname != null && Regex.IsMatch(user.Lastname, query, RegexOptions.IgnoreCase)) ||
                  (user.Username != null && Regex.IsMatch(user.Username, query, RegexOptions.IgnoreCase))
            from label in user.Labels
            where label.Chat == chat || label.Chat == null
            where label.Label != null
            orderby label.Id descending
            select new
            {
                label.Label,
                user.Firstname,
                user.Lastname,
                user.Username,
                label.Chat.Title
            }
        ).Take(50).AsNoTracking().ToAsyncEnumerable();

        return labels.Select(x => new InlineQueryResultArticle(Guid.NewGuid().ToString(),
            $"{GetName(x.Firstname, x.Lastname, x.Username)} ({x.Title})",
            new InputTextMessageContent(GetLabelText(x.Label!, x.Firstname, x.Lastname, x.Username))
            {
                ParseMode = ParseMode.Html
            })
        {
            Description = x.Label
        });
    }

    private static string GetLabelText(string label, string firstName, string? lastName, string? userName)
    {
        StringBuilder builder = new();
        builder.Append("The label of <code>");
        builder.Append(WebUtility.HtmlEncode(GetName(firstName, lastName, userName)));
        builder.Append("</code> is <code>");
        builder.Append(WebUtility.HtmlEncode(label));
        builder.Append("</code>");

        return builder.ToString();
    }

    private static string GetName(string firstName, string? lastName, string? userName)
    {
        StringBuilder builder = new();

        builder.Append(firstName);

        if (!string.IsNullOrWhiteSpace(lastName))
        {
            builder.Append(' ');
            builder.Append(lastName);
        }

        if (userName is not null)
        {
            builder.Append($", @{userName}");

            return builder.ToString();
        }

        return builder.ToString();
    }
}