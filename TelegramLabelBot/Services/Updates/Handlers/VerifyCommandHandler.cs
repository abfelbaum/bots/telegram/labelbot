using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Abstractions;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Services.Updates.Handlers;

public class VerifyCommandHandler : ITelegramUpdate<Message>
{
    private readonly IAdministratorService _administratorService;
    private readonly ITelegramBotClient _client;
    private readonly ICommandHelpGenerator _helpGenerator;

    public VerifyCommandHandler(ITelegramBotClient client, IAdministratorService administratorService,
        ICommandHelpGenerator helpGenerator)
    {
        _client = client;
        _administratorService = administratorService;
        _helpGenerator = helpGenerator;
    }

    public async Task InvokeAsync(Message message, CancellationToken cancellationToken = new())
    {
        StringBuilder builder = new();
        if (message.ReplyToMessage is { } replyToMessage)
        {
            if (replyToMessage.From is not { } user)
            {
                builder.Append("I cannot see the user that message came from. Please try a newer one!");
            }
            else
            {
                if (_administratorService.IsChatAdmin(user))
                {
                    builder.Append($"{user.FirstName} is an administrator!");
                }
                else
                {
                    builder.Append($"{user.FirstName} is not an administrator");
                }
            }
        }
        else
        {
            builder.Append(await _helpGenerator.Generate("verify", message.Chat, message.From, cancellationToken).ConfigureAwait(false));
        }

        await _client.SendTextMessageAsync(message.Chat, builder.ToString(), ParseMode.Html,
            replyToMessageId: message.ReplyToMessage?.MessageId, cancellationToken: cancellationToken).ConfigureAwait(false);
    }
}