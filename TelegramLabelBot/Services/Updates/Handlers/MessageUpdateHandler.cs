using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Telegram.Bot.Types;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Services.Updates.Handlers;

public class MessageUpdateHandler : ITelegramUpdate<Message>
{
    private readonly IAdministratorUpdateTracker _tracker;

    public MessageUpdateHandler(IAdministratorUpdateTracker tracker)
    {
        _tracker = tracker;
    }


    public async Task InvokeAsync(Message message, CancellationToken cancellationToken = new())
    {
        if (message.From is null)
        {
            return;
        }

        await _tracker.Push(message.From, cancellationToken).ConfigureAwait(false);
    }
}