using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace TelegramLabelBot.Services.Updates.Handlers;

public class VersionCommandHandler : ITelegramUpdate<Message>
{
    private readonly IBotService _botUser;
    private readonly ITelegramBotClient _client;

    public VersionCommandHandler(IBotService botUser, ITelegramBotClient client)
    {
        _botUser = botUser;
        _client = client;
    }

    public async Task InvokeAsync(Message message, CancellationToken cancellationToken = new())
    {
        StringBuilder builder = new();

        builder.Append($"{(await _botUser.Get().ConfigureAwait(false)).Username} v{Assembly.GetExecutingAssembly().GetName().Version}");
        builder.AppendLine();
        builder.AppendLine();
        builder.Append(
            @"This bot is an open source project. The source code, licensed under <a href=""https://git.thevillage.chat/Abfelbaum/telegramlabelbot/-/blob/main/LICENSE"">AGPL v3</a>, can be found <a href=""https://git.thevillage.chat/Abfelbaum/telegramlabelbot"">here</a>.");

        await _client.SendTextMessageAsync(message.Chat.Id, builder.ToString(), ParseMode.Html,
            replyToMessageId: message.MessageId, cancellationToken: cancellationToken).ConfigureAwait(false);
    }
}