using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using TelegramLabelBot.Entities;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Services.Updates.Handlers;

public class RemoveLabelCommandHandler : ITelegramUpdate<Message>
{
    private readonly ILabelService _labelService;
    private readonly IAdministratorUpdateTracker _updateTracker;
    private readonly ITelegramBotClient _client;

    public RemoveLabelCommandHandler(IAdministratorService adminService, ILabelService labelService,
        IAdministratorUpdateTracker updateTracker,
        ITelegramBotClient client)
    {
        _labelService = labelService;
        _updateTracker = updateTracker;
        _client = client;
    }

    public async Task InvokeAsync(Message message, CancellationToken cancellationToken = new())
    {
        StringBuilder builder = new();
        User? executingUser = message.From;

        if (executingUser is null)
        {
            return;
        }

        User affectedUser = message.ReplyToMessage?.From ?? executingUser;

        builder.Append(await RemoveLabel(affectedUser, executingUser, message.Chat, cancellationToken).ConfigureAwait(false));
        await _updateTracker.Push(affectedUser, cancellationToken).ConfigureAwait(false);

        await _client.SendTextMessageAsync(message.Chat.Id, builder.ToString(), ParseMode.Html,
            replyToMessageId: message.MessageId, cancellationToken: cancellationToken).ConfigureAwait(false);
    }

    private async Task<StringBuilder> RemoveLabel(User affectedUser, User executingUser, Chat chat, CancellationToken cancellationToken = new())
    {
        StringBuilder builder = new();
        IValidationResult validationResult = _labelService.ValidateRemove(affectedUser.Id, executingUser.Id);

        if (validationResult.Status == ValidationStatus.Failed)
        {
            builder.Append("You cannot remove the label because of the following reasons:");

            foreach (string reason in validationResult.Reasons)
            {
                builder.AppendLine();
                builder.Append($"- {WebUtility.HtmlEncode(reason)}");
            }

            return builder;
        }

        if (!await _labelService.RemoveLabel(affectedUser, chat, executingUser.Id, cancellationToken).ConfigureAwait(false))
        {
            builder.Append("No label found for ");

            if (affectedUser.Username is null)
            {
                builder.Append(WebUtility.HtmlEncode(affectedUser.FirstName));
            }
            else
            {
                builder.Append($"@{WebUtility.HtmlEncode(affectedUser.Username)}");
            }

            return builder;
        }

        builder.Append("Label of ");

        if (affectedUser.Username is null)
        {
            builder.Append(WebUtility.HtmlEncode(affectedUser.FirstName));
        }
        else
        {
            builder.Append($"@{WebUtility.HtmlEncode(affectedUser.Username)}");
        }

        builder.Append(" removed");

        return builder;
    }
}