using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Telegram.Bot.Types;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Services.Updates.Handlers;

public class LabelChatMemberUpdateHandler : ITelegramUpdate<ChatMemberUpdated>
{
    private readonly IAdministratorUpdateTracker _updateTracker;

    public LabelChatMemberUpdateHandler(IAdministratorUpdateTracker updateTracker)
    {
        _updateTracker = updateTracker;
    }

    public async Task InvokeAsync(ChatMemberUpdated chatMember, CancellationToken cancellationToken = new())
    {
        await _updateTracker.ChatMemberUpdated(chatMember, cancellationToken).ConfigureAwait(false);
    }
}