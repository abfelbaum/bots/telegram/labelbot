using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Abstractions;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Services.Updates.Handlers;

public class LockCommandHandler : ITelegramUpdate<Message>
{
    private readonly IAdministratorService _adminService;
    private readonly ILockService _lockService;
    private readonly ITelegramBotClient _client;
    private readonly ICommandHelpGenerator _helpGenerator;

    public LockCommandHandler(IAdministratorService adminService, ILockService lockService, ITelegramBotClient client,
        ICommandHelpGenerator helpGenerator)
    {
        _adminService = adminService;
        _lockService = lockService;
        _client = client;
        _helpGenerator = helpGenerator;
    }

    public async Task InvokeAsync(Message message, CancellationToken cancellationToken = new())
    {
        if (message.From is null)
        {
            return;
        }

        if (message.ReplyToMessage is null)
        {
            StringBuilder? helpText = await _helpGenerator
                .Generate("lock", message.Chat, message.From, cancellationToken).ConfigureAwait(false);

            if (helpText is null)
            {
                return;
            }


            await _client.SendTextMessageAsync(message.Chat.Id, helpText.ToString(), ParseMode.Html,
                cancellationToken: cancellationToken).ConfigureAwait(false);
            return;
        }

        if (!_adminService.IsChatAdmin(message.From))
        {
            const string text = "You need to be an admin (not assigned by me) to use this command";
            await _client.SendTextMessageAsync(message.Chat.Id, text, cancellationToken: cancellationToken)
                .ConfigureAwait(false);
            return;
        }

        if (message.ReplyToMessage.From?.Id is null)
        {
            const string text = "I cannot find the user you replied to. Sorry :(";
            await _client.SendTextMessageAsync(message.Chat.Id, text, cancellationToken: cancellationToken)
                .ConfigureAwait(false);
            return;
        }

        if (_adminService.IsChatAdmin(message.ReplyToMessage.From))
        {
            const string text = "You cannot lock another admin";
            await _client.SendTextMessageAsync(message.Chat.Id, text, cancellationToken: cancellationToken)
                .ConfigureAwait(false);
            return;
        }

        _lockService.Lock(message.ReplyToMessage.From.Id);

        {
            var text =
                $"<code>{message.ReplyToMessage.From.FirstName}</code> cannot edit their label for 5 minutes!";
            await _client
                .SendTextMessageAsync(message.Chat.Id, text, ParseMode.Html, cancellationToken: cancellationToken)
                .ConfigureAwait(false);
        }
    }
}