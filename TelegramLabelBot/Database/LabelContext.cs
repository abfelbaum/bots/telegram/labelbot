using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using TelegramLabelBot.Database.Entities;

namespace TelegramLabelBot.Database;

[SuppressMessage("ReSharper", "AutoPropertyCanBeMadeGetOnly.Global")]
public class LabelContext : DbContext
{
    /// <inheritdoc />
    public LabelContext(DbContextOptions options) : base(options)
    {
    }

    public DbSet<TelegramLabel> Labels { get; set; } = null!;
    public DbSet<TelegramChat> Chats { get; set; } = null!;
    public DbSet<TelegramUser> Users { get; set; } = null!;

    protected override void OnModelCreating(ModelBuilder builder)
    {
        builder.Entity<TelegramUser>()
            .HasGeneratedTsVectorColumn(
                p => p.SearchVector,
                "english",
                p => new { p.Username, p.Firstname, p.Lastname })
            .HasIndex(p => p.SearchVector)
            .HasMethod("GIN");

        builder.Entity<TelegramUser>()
            .Property(b => b.Id)
            .ValueGeneratedNever();
        builder.Entity<TelegramChat>()
            .Property(b => b.Id)
            .ValueGeneratedNever();
    }
}