using System.ComponentModel.DataAnnotations;

namespace TelegramLabelBot.Database.Entities;

public class TelegramLabel
{
    [Key] public long Id { get; set; }

    public string? Label { get; set; }
#pragma warning disable CS8618
    public virtual TelegramChat Chat { get; set; }
    public virtual TelegramUser User { get; set; }
#pragma warning restore CS8618
}