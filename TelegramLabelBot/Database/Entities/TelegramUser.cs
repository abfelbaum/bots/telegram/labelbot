using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using NpgsqlTypes;
using Telegram.Bot.Types;

namespace TelegramLabelBot.Database.Entities;

[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
[SuppressMessage("ReSharper", "ClassWithVirtualMembersNeverInherited.Global")]
[SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
public class TelegramUser
{
    [Key] public long Id { get; set; }

#pragma warning disable CS8618
    public string Firstname { get; set; }
#pragma warning restore CS8618
    public string? Lastname { get; set; }
    public string? Username { get; set; }

#pragma warning disable CS8618
    public virtual List<TelegramChat> Chats { get; set; }
    public virtual List<TelegramLabel> Labels { get; set; }
    public NpgsqlTsVector SearchVector { get; set; }
#pragma warning restore CS8618

    public static TelegramUser FromUser(User user)
    {
        if (user == null)
        {
            throw new ArgumentNullException(nameof(user));
        }

        TelegramUser telegramUser = new();

        telegramUser.Update(user, false);

        telegramUser.Id = user.Id;

        return telegramUser;
    }

    public void Update(User user, bool validate = true)
    {
        Update(this, user, validate);
    }

    // ReSharper disable once ParameterOnlyUsedForPreconditionCheck.Local
    private static void Update(TelegramUser telegramUser, User user, bool validate = true)
    {
        if (telegramUser is null)
        {
            throw new ArgumentNullException(nameof(telegramUser));
        }

        if (user is null)
        {
            throw new ArgumentNullException(nameof(user));
        }

        if (validate && (telegramUser.Id != user.Id))
        {
            throw new InvalidOperationException("User ids must be the same");
        }

        telegramUser.Firstname = user.FirstName;
        telegramUser.Lastname = user.LastName;
        telegramUser.Username = user.Username;
    }
}