using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using Telegram.Bot.Types;

namespace TelegramLabelBot.Database.Entities;

[SuppressMessage("ReSharper", "ClassWithVirtualMembersNeverInherited.Global")]
[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
[SuppressMessage("ReSharper", "UnusedMember.Global")]
public class TelegramChat
{
    [Key] public long Id { get; set; }

    public string? Title { get; set; }

#pragma warning disable CS8618
    public virtual List<TelegramUser> Users { get; set; }
    public virtual List<TelegramLabel> Labels { get; set; }
#pragma warning restore CS8618

    public static TelegramChat FromChat(Chat chat)
    {
        TelegramChat telegramChat = new();

        telegramChat.Update(chat, false);

        telegramChat.Id = chat.Id;

        return telegramChat;
    }

    public void Update(Chat chat, bool validate = true)
    {
        Update(this, chat, validate);
    }

    // ReSharper disable once ParameterOnlyUsedForPreconditionCheck.Local
    private static void Update(TelegramChat telegramChat, Chat chat, bool validate = true)
    {
        if (telegramChat is null)
        {
            throw new ArgumentNullException(nameof(telegramChat));
        }

        if (chat is null)
        {
            throw new ArgumentNullException(nameof(chat));
        }

        if (validate && (telegramChat.Id != chat.Id))
        {
            throw new InvalidOperationException("Chat ids must be the same");
        }

        telegramChat.Title = chat.Title;
    }
}