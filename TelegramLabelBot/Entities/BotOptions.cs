using System;
using System.Collections.Generic;

namespace TelegramLabelBot.Entities;

public class BotOptions
{
    public string CommandName { get; set; } = "label";
    public string Token { get; set; } = null!;
    public int Buffer { get; set; } = 1;

    public IEnumerable<string> DefaultLabels = new List<string>
    {
        "she/her",
        "he/him",
        "they/them",
        "any/all"
    };

    public TimeSpan LockTimeout = TimeSpan.FromMinutes(5);
}