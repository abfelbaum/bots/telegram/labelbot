using System.Collections.Generic;
using Telegram.Bot.Types;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Entities;

public class ChatState : IChatState
{
    public List<ChatMemberAdministrator> Administrators { get; set; } = new();
    public ChatMemberOwner? Owner { get; set; }
    public List<IChatUpdate> Updates { get; set; } = new();
    public void Save(LabelBotContext context)
    {
        Administrators.Clear();
        Administrators.AddRange(context.Administrators);

        Owner = context.Owner;
    }

    public void Apply(LabelBotContext context)
    {
        context.Administrators.Clear();
        foreach (ChatMemberAdministrator administrator in Administrators)
        {
            context.Administrators.Add(administrator);
        }

        context.Owner = Owner;
    }
}