using System;
using System.Collections.Generic;
using System.Text;
using Telegram.Bot.Types;

namespace TelegramLabelBot.Entities;

public static class CallbackUtils
{
    public static string EncodeData(string id, long userId, string? data)
    {
        if (id is null)
        {
            throw new ArgumentNullException(nameof(id));
        }

        byte[] encodedId = Encoding.UTF8.GetBytes(id);
        byte[] encodedUserId = BitConverter.GetBytes(userId);
        byte[] encodedData = data is not null ? Encoding.UTF8.GetBytes(data) : Array.Empty<byte>();

        var bytes = new List<byte>(encodedId);
        bytes.AddRange(encodedUserId);
        bytes.AddRange(encodedData);

        return Convert.ToBase64String(bytes.ToArray());
    }

    public static (long UserId, string Data) DecodeData(string id, string data)
    {
        if (data is null)
        {
            throw new ArgumentNullException(nameof(data));
        }

        byte[] bytes = Convert.FromBase64String(data);

        return (BitConverter.ToInt64(bytes[id.Length..(id.Length + 8)], 0), Encoding.UTF8.GetString(bytes[(id.Length+8)..]));
    }

    public static (long UserId, string Data) DecodeData(this CallbackQuery query, string id)
    {
        if (query.Data is null)
        {
            throw new ArgumentNullException(nameof(query.Data));
        }

        return DecodeData(id, query.Data);
    }

    public static bool IsId(this CallbackQuery query, string id)
    {
        if (query.Data is null)
        {
            throw new ArgumentNullException(nameof(query));
        }

        if (id is null)
        {
            throw new ArgumentNullException(nameof(id));
        }

        return IsId(id, query.Data);
    }

    public static bool IsId(string id, string data)
    {
        if (id is null)
        {
            throw new ArgumentNullException(nameof(id));
        }

        if (data is null)
        {
            throw new ArgumentNullException(nameof(data));
        }

        byte[] bytes = Convert.FromBase64String(data);

        return Encoding.UTF8.GetString(bytes[..id.Length]) == id;
    }
}