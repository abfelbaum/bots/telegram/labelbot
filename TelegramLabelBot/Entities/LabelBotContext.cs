using System.Collections.Concurrent;
using System.Collections.Generic;
using Abfelbaum.Telegram.Bot.Framework.Types;
using Telegram.Bot.Types;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Entities;

public class LabelBotContext : DefaultContext
{
    public IList<ChatMemberAdministrator> Administrators { get; } = new List<ChatMemberAdministrator>();
    public ChatMemberOwner? Owner { get; set; }
}