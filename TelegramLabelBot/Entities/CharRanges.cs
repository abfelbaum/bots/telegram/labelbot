using System.Collections.Generic;

namespace TelegramLabelBot.Entities;

/// <summary>
///     Contains the ranges containing valid symbols for Telegram admin titles
/// </summary>
public static class CharRanges
{
    /// <summary>
    ///     Contains the ranges containing valid symbols for Telegram admin titles
    /// </summary>
    public static readonly IReadOnlyCollection<(int Min, int Length, int Max)> Ranges = new []
    {
        (33, 127, 159), (161, 8, 168), (170, 4, 173), (175, 603, 777), (779, 40, 818), (820, 11, 830),
        (832, 2261, 3092), (3094, 2421, 5514), (5516, 244, 5759), (5761, 397, 6157), (6159, 2033, 8191),
        (8206, 26, 8231), (8240, 12, 8251), (8253, 12, 8264), (8266, 21, 8286), (8288, 130, 8417),
        (8419, 63, 8481), (8483, 22, 8504), (8506, 90, 8595), (8602, 15, 8616), (8619, 367, 8985),
        (8988, 12, 8999), (9001, 166, 9166), (9168, 25, 9192), (9204, 4, 9207), (9211, 199, 9409),
        (9411, 231, 9641), (9644, 10, 9653), (9655, 9, 9663), (9665, 58, 9722), (9727, 1, 9727),
        (9733, 9, 9741), (9743, 2, 9744), (9746, 2, 9747), (9750, 2, 9751), (9753, 4, 9756), (9758, 2, 9759),
        (9761, 1, 9761), (9764, 2, 9765), (9767, 3, 9769), (9771, 3, 9773), (9776, 8, 9783), (9787, 13, 9799),
        (9812, 11, 9822), (9825, 2, 9826), (9828, 1, 9828), (9831, 1, 9831), (9833, 18, 9850), (9852, 2, 9853),
        (9856, 18, 9873), (9877, 1, 9877), (9880, 1, 9880), (9882, 1, 9882), (9885, 3, 9887), (9890, 8, 9897),
        (9900, 4, 9903), (9906, 11, 9916), (9919, 5, 9923), (9926, 2, 9927), (9929, 5, 9933), (9936, 1, 9936),
        (9938, 1, 9938), (9941, 20, 9960), (9963, 5, 9967), (9974, 1, 9974), (9979, 2, 9980), (9982, 4, 9985),
        (9987, 2, 9988), (9990, 2, 9991), (9998, 1, 9998), (10000, 2, 10001), (10003, 1, 10003),
        (10005, 1, 10005), (10007, 6, 10012), (10014, 3, 10016), (10018, 6, 10023), (10025, 10, 10034),
        (10037, 15, 10051), (10053, 2, 10054), (10056, 4, 10059), (10061, 1, 10061), (10063, 4, 10066),
        (10070, 1, 10070), (10072, 11, 10082), (10085, 48, 10132), (10136, 9, 10144), (10146, 14, 10159),
        (10161, 14, 10174), (10176, 372, 10547), (10550, 463, 11012), (11016, 19, 11034), (11037, 51, 11087),
        (11089, 4, 11092), (11094, 1122, 12215), (12217, 13, 12229), (12216, 1, 12216), (12230, 58, 12287),
        (12289, 47, 12335), (12337, 12, 12348), (12350, 601, 12950), (12952, 1, 12952), (12954, 3635, 16588),
        (16590, 5, 16594), (16589, 1, 16589), (16595, 10330, 26924), (26926, 866, 27791), (27793, 27503, 55295),
        (57344, 6625, 63968), (63970, 1309, 65278), (65280, 252, 65531), (65533, 3, 65535)
    };
}