namespace TelegramLabelBot.Entities;

public enum ValidationStatus
{
    Success,
    Failed
}