using System;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Entities.LabelUpdates;

public class RemoveChatAdministratorUpdate : IChatUpdate
{
    public RemoveChatAdministratorUpdate(User user, ChatId chatId)
    {
        User = user;
        ChatId = chatId;
    }

    public User User { get; }
    public ChatId ChatId { get; }
    public LabelUpdateType Type => LabelUpdateType.Remove;

    public Task Apply(ITelegramBotClient client, CancellationToken cancellationToken = new())
    {
        return client.PromoteChatMemberAsync(ChatId, User.Id, cancellationToken: cancellationToken);
    }

    public bool WaitFor(Update update)
    {
        if (update.Type != UpdateType.ChatMember)
        {
            return false;
        }

        if (update.ChatMember is not { } chatMember)
        {
            return false;
        }

        if (chatMember.Chat.Id != ChatId)
        {
            return false;
        }

        if (chatMember.OldChatMember.User.Id != User.Id)
        {
            return false;
        }

        if (chatMember.OldChatMember is not ChatMemberAdministrator)
        {
            return false;
        }

        return true;
    }
}