namespace TelegramLabelBot.Entities.LabelUpdates;

public enum LabelUpdateType
{
    Add,
    Remove,
    Rename
}