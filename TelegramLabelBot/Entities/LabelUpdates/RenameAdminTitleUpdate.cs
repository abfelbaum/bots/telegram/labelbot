using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Entities.LabelUpdates;

public class RenameAdminTitleUpdate : IChatUpdate
{
    public RenameAdminTitleUpdate(User user, ChatId chatId, string title)
    {
        User = user;
        ChatId = chatId;
        Title = title;
    }

    public string Title { get; }

    public User User { get; }
    public ChatId ChatId { get; }
    public LabelUpdateType Type => LabelUpdateType.Rename;

    public Task Apply(ITelegramBotClient client, CancellationToken cancellationToken = new())
    {
        return client.SetChatAdministratorCustomTitleAsync(ChatId, User.Id, Title, cancellationToken);
    }

    public bool WaitFor(Update update)
    {
        if (update.Type != UpdateType.ChatMember)
        {
            return false;
        }

        if (update.ChatMember is not { } chatMember)
        {
            return false;
        }

        if (chatMember.Chat.Id != ChatId)
        {
            return false;
        }

        if (chatMember.NewChatMember.User.Id != User.Id)
        {
            return false;
        }

        if (chatMember.NewChatMember is not ChatMemberAdministrator)
        {
            return false;
        }

        if (chatMember.OldChatMember is not ChatMemberAdministrator)
        {
            return false;
        }

        return true;
    }

    public IEnumerable<Type> DependsOn { get; } = new[] { typeof(AddChatAdministratorUpdate) };
}