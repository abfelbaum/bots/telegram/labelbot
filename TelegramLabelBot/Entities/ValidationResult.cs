using System;
using System.Collections.Generic;
using System.Text;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Entities;

public class ValidationResult : IValidationResult
{
    public ValidationResult(ValidationStatus status, IEnumerable<string> reasons)
    {
        Status = status;
        Reasons = reasons ?? throw new ArgumentNullException(nameof(reasons));
    }

    public ValidationStatus Status { get; }
    public IEnumerable<string> Reasons { get; }

    public static IValidationResult Success() =>
        new ValidationResult(ValidationStatus.Success, Array.Empty<string>());

    public static IValidationResult Failed(params string[] reasons) =>
        new ValidationResult(ValidationStatus.Failed, reasons);

    public static IValidationResult Failed(IEnumerable<string> reasons) =>
        new ValidationResult(ValidationStatus.Failed, reasons);
}