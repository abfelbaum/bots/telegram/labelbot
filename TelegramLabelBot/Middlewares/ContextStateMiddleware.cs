using System.Threading;
using System.Threading.Tasks;
using Middlewares;
using TelegramLabelBot.Entities;
using TelegramLabelBot.Interfaces;

namespace TelegramLabelBot.Middlewares;

public class ContextStateMiddleware : IMiddleware<LabelBotContext>
{
    private readonly IChatStateProvider _stateProvider;

    public ContextStateMiddleware(IChatStateProvider stateProvider)
    {
        _stateProvider = stateProvider;
    }


    public async Task InvokeAsync(LabelBotContext context, NextMiddleware next,
        CancellationToken cancellationToken = new())
    {
        if (context.Chat?.Id is null)
        {
            await next().ConfigureAwait(false);
            return;
        }

        IChatState state = _stateProvider.GetOrInit(context.Chat.Id);

        state.Apply(context);

        await next().ConfigureAwait(false);


        state.Save(context);
    }
}