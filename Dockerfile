FROM registry.git.abfelbaum.dev/abfelbaum/images/dotnet/runtime:6.0
WORKDIR /app
COPY publish/ .
ENTRYPOINT ["dotnet", "TelegramLabelBot.dll"]