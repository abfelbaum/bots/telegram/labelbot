# Changelog

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.1](https://git.abfelbaum.dev/abfelbaum/bots/telegram/labelbot/compare/1.0.0...1.0.1) (2023-04-07)


### Bug Fixes

* ci ([9646ea3](https://git.abfelbaum.dev/abfelbaum/bots/telegram/labelbot/commit/9646ea3cb92c2c17e9f4126ee3f27ef4562f67ad))

## 1.0.0 (2022-11-25)


### Features

* Implement release process ([3dd5a36](https://git.abfelbaum.dev/abfelbaum/bots/telegram/labelbot/commit/3dd5a362e787899e2da2651af9a49bfd3c6840a7))


### Bug Fixes

* **ci:** Docker image version ([82fbf0f](https://git.abfelbaum.dev/abfelbaum/bots/telegram/labelbot/commit/82fbf0ff631f78e8214c08e727a42732eaa2ab03))
