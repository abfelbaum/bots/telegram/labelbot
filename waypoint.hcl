project = "TelegramLabelBot"

app "bot" {
  build {
    use "docker" {
      buildkit           = false
      disable_entrypoint = true
    }

    registry {
      use "docker" {
        image = var.registry_image_name
        tag   = var.registry_image_tag

        username = var.registry_username
        password = var.registry_password
      }
    }
  }

  deploy {
    use "nomad-jobspec" {
      jobspec = templatefile("${path.app}/rwthqueerbot.tpl.nomad")
    }
  }
}

variable "registry_username" {
  type        = string
  default     = "username"
  env         = ["CI_REGISTRY_USER"]
  description = "The username to log in to the docker registry"
  sensitive   = false
}

variable "registry_password" {
  type        = string
  default     = "username"
  env         = ["CI_REGISTRY_PASSWORD"]
  description = "The password to log in to the docker registry"
  sensitive   = true
}

variable "registry_image_name" {
  type        = string
  env         = ["CI_REGISTRY_IMAGE"]
  description = "The docker image name"
  sensitive   = false
}

variable "registry_image_tag" {
  type        = string
  env         = ["CI_COMMIT_TAG"]
  description = "The docker image tag"
  sensitive   = false
}
