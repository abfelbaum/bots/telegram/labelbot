# This should best be equal to project name, and also the binary name without extension. This defines the $${NOMAD_JOB_NAME}
job "rwthqueerbot" {
  datacenters = ["luna"]
  type        = "service"

  update {
    stagger          = "60s"
    max_parallel     = 1
    min_healthy_time = "60s"
    healthy_deadline = "5m"
  }

  # Should probably be equal to the Job Name in most cases. This defines the $${NOMAD_GROUP_NAME}
  group "database" {
    count = 1

    network {
      mode = "bridge"
    }


    service {
      name = "rwthqueerbot-pgsql"
      port = 5432

      connect {
        sidecar_service {}
      }
    }

    volume "data" {
      type            = "csi"
      source          = "rwthqueerbot-pgsql"
      read_only       = false
      attachment_mode = "file-system"
      access_mode     = "multi-node-multi-writer"
    }

    # Should probably be equal to the Job/Group Name in most cases. This defines the $${NOMAD_TASK_NAME}
    task "postgres" {
      driver = "docker"

      config {
        image = "postgres:13.4-alpine"
      }

      volume_mount {
        volume      = "data"
        destination = "/var/lib/postgresql/data/"
      }

      vault {
        env      = false
        policies = ["rwthqueerbot"]
      }

      template {
        destination = "$${NOMAD_SECRETS_DIR}/environment.database.env"
        env         = true
        data        = <<EOF
{{ with secret "kv/data/bots/rwthqueerbot" }}
          POSTGRES_USER={{.Data.data.DB_USER}}
          POSTGRES_PASSWORD={{.Data.data.DB_PASSWORD}}
          POSTGRES_DB={{.Data.data.DB_DATABASE}}
{{ end }}
        EOF
      }
    }
  }

  group "rwthqueerbot" {
    count = 1

    network {
      mode = "bridge"
    }

    service {
      name = "rwthqueerbot"

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "rwthqueerbot-pgsql"
              local_bind_port  = 5432
            }
          }
        }
      }
    }

    task "rwthqueerbot" {
      driver = "docker"

      config {
        image = "${artifact.image}:${artifact.tag}"
      }

      vault {
        env      = false
        policies = ["rwthqueerbot"]
      }

      template {
        destination = "$${NOMAD_SECRETS_DIR}/environment.env"
        env         = true
        data        = <<EOF
{{ with secret "kv/data/bots/rwthqueerbot" }}
          ConnectionStrings__Npgsql="Host={{ env "NOMAD_UPSTREAM_IP_rwthqueerbot_pgsql" }};Database={{.Data.data.DB_DATABASE}};Username={{.Data.data.DB_USER}};Password={{.Data.data.DB_PASSWORD}};"
          Bot__Token={{.Data.data.BOT_TOKEN}}
{{ end }}
        EOF
      }
    }
  }
}