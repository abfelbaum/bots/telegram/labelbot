# Telegram Label Bot

A bot that saves labels for every user and sets them as an admin title.
When the admin limit of 50 users is reached, the bot rotates through the users, so the 50 last users that wrote do have an admin title everytime.

## Setup

Have a look at the nomad file.

## Usage

A few examples of useful commands and/or tasks.

### Commands

A full list of Telegram commands for this bot.

#### `/help`

Shows a list of available commands.

#### `/label`

Set or remove your label. This command name can be configured, so it may not always be the same!

#### `/reset`

Resets the local admin list of the bot. Useful when the initialization or some synchronizations did not work correctly.

#### `/verify`

Use in reply to another user and it will show you if they are a real chat admin or got promoted by the bot.

#### `/version`

Shows some information about the bot.

### Configuration

The bot can be configured via environment variables.

#### `ConnectionStrings__Npgsql`

Type: `string`

Default: `null`

The connection string to your postgresql database.

#### `Bot__Token`

Type: `string`

Default: `null`

The bot token that your bot uses to authenticate against Telegram bot api.

#### `Bot__CommandName`

Type: `string`

Default: `label`

Set the name of the label command.

#### `Bot__Buffer`

Type: `int`

Default: `1`

The buffer of admins the but should leave free. Per default, at least one admin position will be left free.

## Support
You can create issues via GitLab ServiceDesk by sending an email to reply+abfelbaum-bots-telegramlabelbot-5-issue-@git.abfelbaum.dev.

These issues are not visible for other users by default.

## License
The Server and Client project are licensed under AGPL v3. The CommandLine project is licensed under GPL v3.

## Other

Please have a look at https://wiki.abfelbaum.dev/en/projects/general-information.